% Template for NIME 2014
%
% Modified by Baptiste Caramiaux on 25 November 2013
% Modified by Kyogu Lee on 7 October 2012
% Modified by Georg Essl on 7 November 2011
%
% Based on "sig-alternate.tex" V1.9 April 2009
% This file should be compiled with "nime2011.cls"
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\documentclass{nime-alternate}

\begin{document}
%
% --- Author Metadata here ---
\conferenceinfo{NIME'14,}{June 30 -- July 03, 2014, Goldsmiths, University of London, UK.}

\title{Turnector: Tangible Control Widgets For iOS Devices}

\numberofauthors{1} 
\author{
\alignauthor Ed Rutter\\
\affaddr{University of The West of England}\\
\affaddr{Coldharbour Lane}\\
\affaddr{Bristol, England}\\
\email{ed.rutter@uwe.ac.uk}
}
\date{\today}

\maketitle

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract} 

This paper details the development of Turnector, a control system based upon tangible widgets that are manipulated on the touchscreen of an iOS device. Turnector widgets are modelled on rotary faders and aim to connect the user to parameters in their audio software in a manner analogous to the one-to-one control mapping utilised in analogue studio equipment. The system aims to streamline workflow and facilitate hands-on experimentation through a simple and unobtrusive interface. The physical widgets afford the user freedom to look away from the touchscreen whilst staying in control of multiple parameters simultaneously.

Related work in this area, including interaction design and TUIs in the context of musical control, is discussed in Section \ref{sec:relatedWork} followed by an overview of the system design in Section \ref{sec:systemDesign}. Section \ref{sec:tangibleDesign} presents the requirements and methods involved in the creation of tangible widgets. Section \ref{sec:applicationDevelopment} details the software development, including widget detection, tracking, GUI design, and overall program design. In section \ref{sec:results} the current findings of the project are presented.
%CHECK ALL SECTIONS ARE MENTIONED AND ALL DESCRIPTIONS ARE ACCURATE!!!
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\keywords{Tangible user interface, human-computer interaction, geometric detection, physical-digital interface, MIDI, capacitive touchscreen}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction} \label{sec:introduction}
Touchscreens form the main interface mechanism for an increasing number of portable electronic devices. They provide a versatile, adaptable system for visualising and interacting with software. The touchscreen provides adequate control in many situations, but for precise music control, performers must observe their device interactions closely due to the lack of tactile feedback from the device.

Music performers, engineers and composers are growing less dependant on dedicated synthesis and signal processing hardware and more reliant on general-purpose computer based based audio software. Technological and computational advances facilitate high quality, accurate approximations of analogue units using Digital Signal Processing (DSP), allowing composers and performers to work entirely within a computer environment. This has meant that a traditional music studio is becoming less necessary. Studio equipment, particularly mixing desks, usually provide individual controls for every parameter, and in turn demand a large amount of space. Although the processing capabilities of general purpose computers and dedicated hardware are comparable, application specific control interfaces are being lost. This has an effect on the way music is produced, resulting in composers reliance on automated parameter control. The decline of dedicated tangible control system means more automation is being drawn on screen and live, natural human input is being lost.

\begin{figure}[htbp]
\centering
\includegraphics[width = \columnwidth]{Images/Concept_02.png}
\caption{Turnector control system concept}
\label{fig:ConceptImage}
\end{figure}
%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\section{Related Work} \label{sec:relatedWork}
\subsection{Interaction design}
The notion of using rotary faders to change parameters is orthodox. This is often an indirect manipulation as parameters governing audio processes are abstract attributes that have no physical analogues\cite{Lindeman:Bimanual-PassiveHaptic-3DWidget}. An example of this is a parametric equaliser which usually has three rotary faders to control the frequency, gain and Q. In the context of traditional musical instruments, it is impossible to alter these tonal parameters independently. It has become common to have instruments completely decoupled from their control mechanisms, particularly in electronic music production \cite{dEscrivan:MusicTechnology}\cite{jorda:reacTable}. An example of this is a MIDI keyboard controlling a synthesiser.



%---------%---------%---------%---------%---------%---------%---------%---------%---------%---------
\subsection{Tangible User Interfaces (TUIs)}
%This section needs work !!! FIND REFERENCES!!!
Tangibles that mimic existing control interfaces and widgets enable knowledge transfer, which can help acclimatise users to new systems. Creating an analogy to the controls on music studio hardware affords instant familiarity with the new interface.

The core principles of direct manipulation \cite{rogers:InteractionDesign} are important to follow when designing a tangible interface. Utilising these principles enables understanding of the system to be gained quickly and intuitively, which creates confident users and reduces anxiety. The direct manipulation of virtual Graphical User Interface (GUI) parameters using tangibles is just one layer of control. The virtual rotary faders are indirectly controlling audio processes. Isotonic devices, for example reacTable\cite{jorda:reacTable} widgets, provide constant resistance and variable position and are well suited to the control of GUIs based on direct manipulation. \cite{Lindeman:Bimanual-PassiveHaptic-3DWidget}.

\subsection{Comparable systems}
\subsubsection{reacTable}
reacTable \cite{jorda:reacTable} provides a model of synergy between music and Human-Computer Interaction (HCI). The public response, ease of use and addictive interaction indicate future growth of this type of tangible interface. Jord{\'a} et al. explore the differences between traditional music performance, where one musician affects the many nuances of a single instrument, and laptop performances, where the artist conducts higher level alterations affecting of many sounds simultaneously \cite{jorda:reacTable}.

\subsubsection{Squeezy}
The tangible widget, Squeezy \cite{wang:Squeezy}, developed by Wang et al. demonstrates two methods of adding additional dimensions of input control to a location based touch device. Additional rotation and pressure sensitivity controls are provided through a squeezy ball placed on the surface of a touchscreen. The paper also explains the requirement for at least three touchpoints to detect 360° rotations.

\subsubsection{Hexler: TouchOSC}
TouchOSC \cite{hexler:TouchOSC}, shown in Figure \ref{fig:TouchOSC_and_Scratch2Go}, is a customisable control system for iOS and Android devices. It facilitates easy creation of a control interface through its TouchOSC Editor \cite{hexler:TouchOSC} desktop application. It is versatile but provides little in the way of tactile feedback, meaning that without large amounts of practice and coordination, the user must observe the screen to accurately and reliably adjust parameters. 

\subsubsection{Ion: Scratch2GO}
Figure \ref{fig:TouchOSC_and_Scratch2Go} also displays the recent Ion Scratch2Go \cite{ion:Scratch2Go} widget set, that provides tangible controls to be used on the screen of an iOS device with compatible software. These widgets are placed over virtual faders and jog wheels to allow manipulation of the parameters. These widgets provide tactile feedback, and combined with TouchOSC \cite{hexler:TouchOSC}, create a stable control system. The main drawback of this system is the inability to instantly alter the patch layout on the device.

\subsubsection{Mrmr}
A similar app to TouchOSC\cite{hexler:TouchOSC}, in terms of musical control, is Mrmr \cite{redlinger:mrmr}. This multi-platform application provides the user with a customisable wireless control system that can be edited on the device itself. Mrmr \cite{redlinger:mrmr} is an open source project, so support and recognition of Turnector widgets could be implemented.

%Try to condense the reviews of commercial products!!!

\begin{figure}[htbp]
\centering
\includegraphics[width = \columnwidth]{Images/TouchOSC_and_Scratch2Go.png}
\caption{TouchOSC \cite{hexler:TouchOSC} customisable control system  and Scratch2Go \cite{ion:Scratch2Go} capacitive touch widgets}
\label{fig:TouchOSC_and_Scratch2Go}
\end{figure}


%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\section{System Design} \label{sec:systemDesign}
Turnector focuses on giving as much control and freedom to the user as possible. This is done in a simple manner, without becoming intrusive or distracting the user from the task at hand. When using an analogue mixing desk there is generally a one-to-one mapping of interface controls. Digital Audio Workstations (DAWs) provide all the functionality of an analogue system but lack the comprehensive one-to-one physical control. DAWs possess many control options, but they are hidden away within menus and subwindows. This abstraction can create a hindrance to the creativity and workflow of composers or performers. 

Gelineck et al. \cite{gelineck:SmartTangibleMixing} discuss the need for better mixing control systems to take full advantage of DSP in audio production. In contrast to the system created by Gelineck et al. \cite{gelineck:SmartTangibleMixing}, this project seeks to create widgets analogous to the controls found on studio hardware but for use on the surface of a touchscreen device. The main controls found on hardware interfaces are buttons and linear or rotary faders. This project has focused on the development of rotary faders as they are the most versatile when used in conjunction with a touchscreen and provide the most dimensions of parametric control.

\begin{figure}[htbp]
\centering
\includegraphics[width = \columnwidth]{Images/Demo.png}
\caption{Turnector prototype widget in use}
\label{fig:Demo}
\end{figure}

\subsection{Design Specification}
From the study of other similar systems, both old and new, along with constraints of the project, a set of design specifications was devised:
\begin{itemize}
    \item Multi-dimensional controls (3D) with direct, One-to-one control mapping of parameters.
    \item Different distinguishable tangibles.
    \item Wireless communication to a computer system.
    \item Can be operated with device outside of central vision.
    \item Clear visual feedback.
\end{itemize}

The iOS platform was selected for the project as it is the most popular platform \cite{segundo:tabletMarket} and Apple also has a well documented Software Development Kit (SDK). Figure \ref{fig:ConceptImage} shows a visual representation of how the Turnector system works.

%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\section{Tangible Design} \label{sec:tangibleDesign}

To create the widgets, it was important to understand how an iOS touchscreen functions. iOS devices are designed to recognise finger contact, so an investigation was made into how this interaction could be simulated. As detailed in Barrett and Omote's article \cite{barret:pro-CapTouchTech}, iOS devices use projected capacitance (pro-cap), more specifically mutual-capacitance touchscreens which potentially allow for an unlimited number of contact points. When a finger touches or moves into close proximity of the touchscreen, the user's body capacitance is added to the Resistor-Capacitor (RC) network of that part of the screen. This change in the RC network is interpreted by the Integrated Circuit (IC) in the device to calculate the coordinates of the touchpoint. To create tangible widgets that function on this type of screen, there are a few options:
\begin{itemize}
  \item Passive widgets that are made of conductive material to facilitate charge transfer when touched, in the same way that a stylus does.
  \item Active widgets \cite{wang:Squeezy} that utilise electronic systems to alter the RC network.
  \item Passive Untouched Capacitive Widgets (PUCs) \cite{voelker:PUCs} that facilitate charge transfer to another part of the device itself.
\end{itemize}

Active widgets were dismissed early on in the development process due to the hardware complexity involved. Experimentation thus began with conductive materials to simulate touchpoints. Firstly, a stylus was investigated and it was noted that the soft tip was not recognised until pressure was applied that increased the area of contact. Contact area was obviously a critical factor so an investigation into the smallest recognisable contact point was undertaken. Aluminium was machined to create different size contacts for testing. This material was used as it is easy to machine and is an adequate conductor. For the purpose of the investigation, circular contact points were used for ease of machining and because they approximately represent the elliptical contact shape of a finger. Contacts with a diameter of 2-10mm (+/- 0.05mm) in 1mm increments were created and tested on the screen. It was found that a contact point smaller than 5mm diameter was not detected; with the use of a screen protector a contact point of at least 6mm was required.

%ADD MAX WAS OVER 12MM BUT NO LARGER WAS TESTED

\begin{figure}[htbp]
\centering
\includegraphics[width = \columnwidth]{Images/Prototype_Jig_03.png}
\caption{Prototype widgets in a calibration jig.}
\label{fig:Prototype_Jig}
\end{figure}

An software application was written to allow detection and analysis of points, detailed in Section \ref{sec:applicationDevelopment}. To complement the software prototype, hardware prototypes were also built, at first by attaching styluses together in various two contact point arrangements, and later three contact point arrangements. These provided confirmation that the system could work but to further the development, more solid, refined and adjustable widgets were constructed. Firstly a baseplate was machined in aluminium. Three pairs of contact assemblies that screw together through the baseplate were also machined in aluminium, with 5mm diameter circular contact points. The design allowed a range of geometrically different contact point arrangements to be created, without the need of tools or further machining. A calibration jig, shown in Figure \ref{fig:Prototype_Jig}, was made to allow accurate, reproducible arrangements to be configured. Some of the possible contact point configurations can be seen in Figure \ref{fig:Knob-ContactPoints}. The position of the points, and therefore shape of the widget, allow the user tactile feedback to differentiate between them without visual aid. In the final iteration the shape of the rotary fader or other tactile device will be used to allow differentiation between the widgets. 

\begin{figure}[htbp]
\centering
\includegraphics[width = \columnwidth]{Images/Knob.png}
\caption{A possible design for a set of Turnector rotary faders (conductive material in red, non-conductive in grey) with contact points arranged using an 11 point polar array. }
\label{fig:Knob-ContactPoints}
\end{figure}


%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\section{Application Development} \label{sec:applicationDevelopment}
The application was developed predominantly in Objective C, using core frameworks from the Apple SDK to allow forward and backward compatibility. Apple's gesture recognition classes were trialled as a method for translating touch data into musical control data, but did not grant the degree of flexibility and expandability required. These classes allowed two points to be used to monitor the positional movements (XY) and rotation (Z) through an affine transform, with the distance between the points giving the tangible an ID. Using these methods meant that only a small number of distinguishable tangibles could be created before either the touchpoints became too close together to be identifiable, or the widget size had to be increased. Another factor was that tangibles with contact points close together provide less precise rotation tracking, due to inherent inaccuracies in touchpoint detection. This method was sufficient to test the system but would not fulfil the goal of creating an expandable elegant product.

%---------%---------%---------%---------%---------%---------%---------%---------%---------%---------
\subsection{Tangible Detection Algorithm}
The tangible detection algorithm analyses touch location data created when the contact points of the widget come into contact with the touchscreen and the rotary fader is in contact with the user.

Figure \ref{fig:DetectionAlgorithm} helps to illustrate the algorithm. First, the contact points are detected. If the number of touchpoints is three, then vectors between them are calculated. The magnitude of the vectors are compared with stored values and if a match is found, a known tangible is detected. Once a known tangible has been detected the circumcentre of the 3 points is calculated, this is taken as the centre of rotation and the centre for the GUI element displayed. One of the contact points is then selected as the rotation reference point. The points are tracked, and a data stream of XYZ values are created as the widget is manipulated on the touchscreen; where X and Y are the Cartesian coordinates of the widget with respect to the screen and Z is its rotation angle.

\begin{figure}[htbp]
\centering
\includegraphics[width = \columnwidth]{Images/DetectionAlgorithm_01.eps}
\caption{Detection algorithm illustration}
\label{fig:DetectionAlgorithm}
\end{figure}

\subsection{Control and Communication}
The XYZ values are scaled and offset to values between 0.0 and 1.0 to allow easy mapping onto other control ranges. Next the values are translated into 14bit MIDI messages ready for transmission. Control data is transferred using core MIDI to allow ease of communication with standard music systems. CoreMidi makes use of Zero-configuration networking (Zeroconf),and is consequently quick and easy to set up using only native OSX utilities.


%---------%---------%---------%---------%---------%---------%---------%---------%---------%---------
\subsection{Feedback and Graphical User Interface}
A clean minimal interface was designed to give easy to comprehend feedback without distracting the user from their work. Turnector shares the visual design ideologies as reacTable \cite{jorda:reacTable}, elegantly visualising information whilst avoiding any purely decorative elements. The core GUI elements are coloured arcs that are displayed around the physical widgets, as shown in Figure \ref{fig:ConceptImage}. Different widgets are displayed with different colours. The XY position on the touchscreen can be detected both kinaesthetically and visually. It is more difficult to determine the widgets orientation so the GUI is used to supplement the haptic feedback.

The GUI gives the user feedback from all three dimensions of control. An arc is displayed around the widget, its centre position governed by the circumcentre of the widgets contact points and its angle controlled by the rotation of the widget. The expanding arc exhibits obvious parallels to rotary inputs seen in both hardware and software. Widget movements on the X axis alter the arcs opacity with the radius of the arc affected by the Y axis. These subtle feedback devices are used to subconsciously reinforce the user's confidence in the system but without distraction from their workflow.

%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\section{Results} \label{sec:results}
Contact points of 6mm can accurately and reliably be detected. As the touchpoint circumcircle radius is reduced below 30mm the level of accuracy decreases due to precision errors in the touchscreen. With larger radii tangibles begin to function as PUCs.

An initial user study was conducted in a quiet university lab. Three widgets were mapped to separate bands on a parametric equaliser in Ableton Live \cite{ableton:Live}. The X and Y coordinates mapped to frequency and gain with rotation mapped to Q. The participants, who were already conversant with DAWs, were able to understand and use the system in less than a minute.

%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\newpage
\section{Conclusion and Future Development} \label{sec:conclusion}
This paper has presented Turnector which is a widget based system used to interface with a computer. The research of similar systems and observation of others using Turnector has shown that tangible control systems are an effective tool for HCI.

The future of the project is focused on improving the tracking algorithm to allow the use of multiple widgets simultaneously. New widget prototypes will also be developed with defined tactile differences for use in formal user studies to enable reliable performance and usability data to be gained.


%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=%=
\section{Acknowledgement}
%Chris Nash, Chris Hart, Ralph Rutter, 

% The following two commands are all you need in the
% initial runs of your .tex file to
% produce the bibliography for the citations in your paper.
\bibliographystyle{abbrv}
\bibliography{nime-references}  % sigproc.bib is the name of the Bibliography in this case
% You must have a proper ".bib" file
%  and remember to run:
% latex bibtex latex latex
% to resolve all references
%
% ACM needs 'a single self-contained file'!
%
%%% Place this command where you want to balance the columns on the last page. 
%\balancecolumns 
\end{document}
